CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Instructions
 * Maintainers


INTRODUCTION
-------------
Commerce shipping weight tariff

Allows the creation of complex matrices of shipping tariffs by order weight,
only for "Shipping by weight tariff" shipping method.
For example:
                   < .5kg | < 2kg | < 10kg
Base rate: £2  +   |   £2   |  £4   |  £6
Calculated rate:   |   £4   |  £6   |  £8

Conditions are stored in a plugin "Shipment weight - Multiple Conditions".
It stores the next 3 configurable fields for each section:
 - a weight field for the maximum weight for the tariff
 - a price per weight
 - a condition operator.


REQUIREMENTS
-------------

Drupal Commerce 2.x - https://www.drupal.org/project/commerce
Commerce Shipping 2.x - https://www.drupal.org/project/commerce_shipping
Physical fields - https://www.drupal.org/project/physical


INSTALLATION
------------

Install as you would normally install a contributed Drupal module. Visit
https://www.drupal.org/docs/8/extending-drupal-8/installing-drupal-8-modules
for further information.


CONFIGURATION
-------------

The module configuration is similar to default commerce
shipping method configuration.


INSTRUCTIONS
-------------

To use this shipping method just try to follow next steps:
1. Create a new shipping method here:
   "/admin/commerce/config/shipping-methods/add"
2. Select "Shipping by weight tariff" plugin.
3. Enter plugin label.
4. Set base rate amount for shipping method.
5. Open "Shipment" conditions list below.
6. Select "Shipment weight - Multiple Conditions" plugin.
7. Configure your conditions.
8. Save shipping method configuration.

Other shipping method options you can configure as neeeded.


MAINTAINERS
-----------

Current maintainers:
 * Joachim Noreiko (joachim) - https://www.drupal.org/u/joachim
 * Yaroslav Samoilenko (ysamoylenko) - https://www.drupal.org/u/ysamoylenko
